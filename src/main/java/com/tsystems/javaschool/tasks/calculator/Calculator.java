package com.tsystems.javaschool.tasks.calculator;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */


    private List<String> standartOperators =
            Stream.of("(", ")", "+", "-", "*", "/").collect(toList());

    protected String evaluate(String statement) {


        Stack<String> stack = new Stack<>();
        Queue<String> queue = new ArrayDeque<>(ConvertToPostfixNotation(statement));
        if (queue.size() == 0) {
            return null;
        }
        String str = (((ArrayDeque<String>) queue).removeFirst());

        double summ = 0;
        while (queue.size() >= 0) {
            if (!standartOperators.contains(str)) {
                stack.push(str);
                str = ((ArrayDeque<String>) queue).removeFirst();
            } else {

                try {

                    switch (str) {

                        case "+": {
                            double a = Double.valueOf(stack.pop());
                            double b = Double.valueOf(stack.pop());
                            summ = a + b;
                            break;
                        }
                        case "-": {
                            double a = Double.valueOf(stack.pop());
                            double b = Double.valueOf(stack.pop());
                            summ = b - a;
                            break;
                        }
                        case "*": {
                            double a = Double.valueOf(stack.pop());
                            double b = Double.valueOf(stack.pop());
                            summ = b * a;
                            break;
                        }
                        case "/": {
                            double a = Double.valueOf(stack.pop());
                            double b = Double.valueOf(stack.pop());
                            if (a == 0) return null;
                            else summ = b / a;
                            break;
                        }

                    }
                } catch (Exception ex) {
                    System.out.println((ex.getMessage()));
                    return null;
                }
                stack.push(summ + "");
                if (queue.size() > 0)
                    str = ((ArrayDeque<String>) queue).removeFirst();
                else
                    break;
            }

        }
        if (summ == (long) summ)
            return String.format("%d", (long) summ);
        else
            return String.format("%s", summ);


    }

    private List<String> separate(String statement) {
        int pos = 0;
        ArrayList<String> result = new ArrayList<>();
        while (pos < statement.length()) {
            String str = "";
            str += statement.charAt(pos);
            if (!standartOperators.contains(statement.charAt(pos))) {
                if (Character.isDigit(statement.charAt(pos)))
                    for (int i = pos + 1; i < statement.length() &&
                            (Character.isDigit(statement.charAt(i)) || (statement.charAt(i) == ',') || (statement.charAt(i) == '.')); i++) {
                        str += statement.charAt(i);
                    }
                else if (Character.isLetter(statement.charAt(pos)))
                    for (int i = pos + 1; i < statement.length() &&
                            (Character.isLetter(statement.charAt(pos)) || Character.isDigit(statement.charAt(i))); i++)
                        str += statement.charAt(i);
            }
            result.add(str);
            pos += str.length();
        }
        return result;
    }

    private int getPriority(String s) {
        switch (s) {
            case "(":
            case ")":
                return 0;
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            /*case "^":
                return 3;*/
            default:
                return 4;
        }
    }

    private List<String> ConvertToPostfixNotation(String input) {
        List<String> outputSeparated = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        if (input == null) return new ArrayList<>();
        List<String> string = separate(input);
        try {
            for (String str : string) {
                if (standartOperators.contains(str)) {
                    if (stack.size() > 0 && !str.equals("(")) {
                        //if (!(stack.size() > 0) && string.get(i).equals(")")) return new ArrayList<>();
                        if (str.equals(")")) {
                            String s;
                            //if ((stack.size() > 0)) {
                            s = stack.pop();
                            //} else return new ArrayList<>();
                            while (!s.equals("(")) {
                                outputSeparated.add(s);

                                s = stack.pop();

                            }
                        } else if (getPriority(str) > getPriority(stack.peek())) // было >
                            stack.push(str);
                        else {
                            while (stack.size() > 0 && getPriority(str) <= getPriority(stack.peek())) // было <
                                outputSeparated.add(stack.pop());
                            stack.push(str);
                        }
                    } else
                        stack.push(str);
                } else
                    outputSeparated.add(str);
            }
        } catch (Exception ex) {
            return new ArrayList<>();
        }

        while (stack.size() > 0) {
            if (stack.contains("(") || stack.contains(")")) return new ArrayList<>();
            outputSeparated.add(stack.pop());
        }
        return outputSeparated;
    }


}
