package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    protected int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        try {
            inputNumbers.sort(Comparator.comparingInt(Integer::byteValue));
        }
        catch (Error ex) {
            throw new CannotBuildPyramidException();
        }
        double rows = (-1 + Math.sqrt(1 + 8 * inputNumbers.size())) / 2; // выводится из формулы суммы арифметический прогрессии. Смотри Фигурные числа в википедии
        if (rows != (long) rows) {
            throw new CannotBuildPyramidException();
        }
        int columns = (int) rows * 2 - 1;
        int[][] result = new int[(int) rows][columns];
        int count = 0;
        for (int i = 0; i < rows; i++) {
            for (int k = 0; k < columns; k++) {
                if (k >= ((columns - 1) / 2 - i) && k <= ((columns - 1) / 2 + i)) {
                    if ((i + k)%2!=rows%2 ) {
                        result[i][k] = inputNumbers.get(count);
                        count++;

                    }

                }
            }
        }


        return result;
    }


}
