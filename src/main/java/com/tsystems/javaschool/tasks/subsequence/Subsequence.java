package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    protected boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        if (x.isEmpty()) return true;
        if (y.isEmpty()) return false;
        for ( int k = 0, i =0; k<y.size(); k++) {
            if (x.get(i) == y.get(k)) {
                i++;
            }
            if (i==x.size()) return true ;
            }

        return  false;


    }
}
